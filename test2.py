from math import *
from collections import Counter
import re

def main():
    username = input("Entrez votre nom")
    print("Bienvenue à " + username)

    a = int(input("Veuillez entrez un nombre a"))
    b = int(input("Veuillez entrez un nombre a"))
    c = int(a+b)
    print(c)



def ep2bis():
    a = int(input("Veuillez entrez un nombre a"))
    b = int(input("Veuillez entrez un nombre b"))
    if b > a:
        print(b)
    else:
        print(a)

    c = int(input("Veuillez entrez un nombre c"))
    if c%2 == 0:
        print("c'est pair")
    else:
        print("c'est impair")

    d = int(input("Saisissez votre âge"))

    if d >= 18:
        print("Vous êtes majeur.")
    else:
        print("Vous êtes mineur")

    e = int(input("Premier Chiffre"))
    f = int(input("Deuxieme Chiffre"))
    g = int(input("Troisieme Chiffre"))
    g = [e, f, g]
    print(max(g))

def ep3():
    a = 0
    while a <= 100:
        print(a)
        a += 1
    b = int(input("Choissisez votre chiffre"))
    result = 0
    for i in range(0,b+1):
        result += i
    print(result)
    c = int(input("Choisissez un entier"))
    print(c)
    d = int(input("Choissez un rayon pour votre cercle"))
    perimetre = int(2 * pi * d)
    aire = int(pi * d ** 2)
    print(perimetre,aire)
    f = int(input("Choississez votre chiffre pour connaitre les diviseurs"))
    g = []

    for i in range (1,f + 1):
        if f % i == 0:
            g.append(i)
    print(g)

def ep3part2():

        a = []
        n=1
        for i in range(1, 10):
            print(n, " x ", i, " = ", n * i)

        for j in range(1, 10):
            for i in range(1, 10):
                print(j, " x ", i, " = ", j * i)

        b = int(input("Choissez votre chiffre a diviser"))
        c = int(input("Choissez votre diviseur"))
        quotient = int(b // c)
        reste = int(b % c)
        print("Le quotient de ",b, " et ",c,"est égale à", quotient,"et le reste est de ", reste)

        d = int(input("Choisissez le nombre de votre choix"))
        if int(sqrt(d)) ** 2 == d :
            print("Le nombre est un carré parfait")
        else:
            print("Le nombre n'est pas un carré parfait")

        e = int(input('Écris un nombre entier'))
        i = 2
        while i < e and e % i != 0:
            i = i + 1
        if i == e:
            print(e, "est premier")
        else:
            print("Ce n'est pas un nombre premier.")

def ep4():
    chainea = "python"
    for i in range(len(chainea)):
        print(chainea[i])

    chaineb = "itescia"
    freqb = Counter(chaineb)
    for (a, b) in freqb.items():
        print("Nombre d'occurrences de ", a, " est : ", b)

    chainec = input("Ecrire une chaine de caractère")
    for i in range(len(chainec)):
        if(chainec[i] == "a"):
            print("Le caractère 'a' se trouve à la position : ", i)


    liste = ["laptop", "iphone" , "tablet"]
    for i in range (len(liste)):
        print(liste[i])
        print(len(liste[i]))

    chainef = input("Ecrire une chaine")
    long = len(chainef)
    pc = chainef[0]
    lc = chainef[long-1]
    ch1 = chainef[1:long - 1]
    ch2 = lc + ch1 + pc
    print(ch2)



    chained = input("Ecrire une chaine de caractère")
    listev = ["a", "e", "i", "o", "u", "y"]
    nbv = 0
    for i in range(len(chained)):
        if chained[i] in listev:
            nbv+=1
    if nbv == 0:
        print("Il n'y a pas de voyelles dans la chaine " + chained )
    else:
        print("La chaine de caractère " + chained + " contient " + str(nbv) + " voyelles.")

def ep4part2():

    texte1 = "Python est un merveilleux langage de programmation"
    n = 0
    ind = texte1.index(" ")
    l = []

    for i in range(0, 1):
        l.append(texte1[n:ind])
        print(l)

    texte2 = input("Inserez un fichier")
    result = texte2[-4:]
    print(result)

    texte3 = input("Ecrire une chaine")
    for i in range(len(texte3) // 2):
        if texte3[i] != texte3[-i - 1]:
            a = 1
        else:
            a = 0

    if a == 0:
        print("C'est un palindrome")
    else:
        print("Ce n'est pas un palindrome")

    texte4 = input("Une chaine a mettre la")
    textereverse = ""
    i = len(texte4) - 1
    while i >= 0:
        textereverse += texte4[i]
        i -= 1
    print(textereverse)


    texte5 = input("Mettre une chaine de caractere")
    texte5 = texte5.split()
    for i in texte5:
        if(i[0] == "a"):
            print("Le mot", i, "commence par 'a'")

def ep5():
    chainea = input("Mettre une chaine de caractere")
    l = chainea.split()
    mot = ""
    for i in l:
        if(len(mot)<len(i)):
            mot = i
    print("Le mot le plus long est", mot)

    chaineb = input("Mettre une chaine")
    if len(chaineb) == 0:
        print("la liste est vide")
    else:
        print("la liste n'est pas vide")

    list1 = [1, 2, 6, 5]

    list2 = [1, 3, 4, 6]

    list1set = set(list1)

    intersection = list1set.intersection(list2)

    interlist = list(intersection)

    print("les valeurs communes sont", interlist)

    list3 = [1, 2, 3, 4, 5, 6]
    lp = []
    lu = []
    for i in range(len(list3)):
        if i%2 == 0:
            lp.append(i)
        else:
            lu.append(i)
    print("Ces nombres sont pairs", lp, "     Ces nombres sont impairs",lu)

    list4 = "Python"
    l4 = list4[::2]
    print("Ca donne", l4)

    notes = [12, 4, 14, 11, 18, 13, 7, 10, 5, 9, 15, 8, 14, 16]
    l = []
    for i in range(len(notes)):
        if notes[i] >= 10:
            l.append(i)
    print(" les notes au dessus de la moyenne sont: ", l)

    texte = "Bonsoir on compte on compte bonsoir alalalal azedazd"
    txt = texte.split()
    cnt = Counter()

    for i in txt:
        if i != "":
            cnt[i] += 1
    print(cnt)


    s = re.sub(' +', ' ', 'Est    ce que ca     marche   ')
    print(s)

    s1 = input("premiere chaine")
    s2 = input("deuxieme chaine")
    s1 = s1.split()
    s2 = s2.split()


    s1set = set(s1)

    intersection = s1set.intersection(s2)

    interlist2 = list(intersection)

    print("les valeurs communes sont", interlist2)

    s = "Pyhon est un langage de programmation"
    s = s.split()
    print(" ".join([s[-1]] + s[1:-1] + [s[0]]))

    s = "Pyhon est un langage de programmation"
    s = s.split()
    result = len(s)
    print("ca fait", result)

def nombredivibles(l, n):

    nombresDivisibles = []
    for i in l:
        if i % n == 0:
            nombresDivisibles.append(i)

    print(nombresDivisibles)

def nombreOccurence(l,x):
    n = 0
    for s in l:
        if x in s:
            n += 1
    return n

def InsertEtoile(a):
    x = "*".join(a)
    return x

def toutEnMajuscule(a):

    [a.upper() for a in []]
    []

def toutEnMajuscule(l):
    list = []

    for s in l:
        list.append(s.upper())

    return list

def countUpperAndLower(s):
    r = {'countUpper': 0, 'countLower': 0}

    for c in s:
        if c.islower():
            r['countLower'] += 1

        if c.isupper():
            r['countUpper'] += 1

    return r



def base10(nb, base=10):
    if nb == 0:
        yield 0
    while nb:
        nb, d = divmod(nb, base)
        yield d






def Intersec(s1, s2):
    s1 = s1.split()
    s2 = s2.split()

    s1set = set(s1)

    intersection = s1set.intersection(s2)

    interlist2 = list(intersection)

    print("les valeurs communes sont", interlist2)



def chiffrePorteBonheur(nb):
    s = 0
    if int(nb) > 10 :
        for i in nb:
            s += int(i) ** 2
        nb = s
        return chiffrePorteBonheur(str(nb))
    elif int(nb) == 1 :
        print("Le chiffre " + nbChoix + " est un chiffre porte bonheur")
    else :
        print("Le chiffre " + nbChoix + " n'est pas porte bonheur")


def generator(n):
    for i in range(ord('a'), ord('z') + 1):
        for k in range(0, n):
            print(chr(i))

def powerset(n):
    if len(n) <= 1:
        yield n
        yield []

    else:
        for item in powerset(n[1:]):
            yield [n[0]]+item
            yield item





def powerset(seq):

    if len(seq) <= 1:
        yield seq
        yield []
    else:
        for item in powerset(seq[1:]):
            yield [seq[0]]+item
            yield item
            
def decorateur(func):
    def helper(*args, **argv):
        helper.calls += 1
        return func(*args, **argv)

    helper.calls = 0
    return helper


@decorateur
def decorer(x):
    return x + 1

if __name__ == '__main__':
    s = 0
    nbChoix = (input("Entrer un nombre : "))
    chiffrePorteBonheur(nbChoix)
    generator(int(input("Inserez un nombre")))
    print([x for x in powerset([1, 2,3])])
    print(decorer(0))
    print(decorer(1))
    print("number of calls:", decorer.calls)